# Tools for HR Departments #

Over the years, it’s become apparent to me that HR departments could use some 
help on the security front. The cannonical example is a breakdown in 
communications with IT that results of former employees being left on systems. 
More subtle are issues with the hiring process: I’ve seen jobs advertised 
looking for admins for old, unsupported, public-facing Web applications. And 
too much detail about software which, combined with work shift requirements to 
supply timing information, can provide useful information to an attacker.

Is it possible to increase precision, without a downside? One possibility 
involves standardizing, if only somewhat, titles, responsibilities, and 
typical tasks. Here the U.S. federal government may actually be of some help. 
Though they don’t make it easy.

If you visit 
https://niccs.us-cert.gov/workforce-development/cyber-security-workforce-framework 
(part of DHS National Initiative For Cybersecurity Careers And Studies) you 
find tons of information. Seven categories, with a varying number specialty 
areas under each, Drilling down further, you find lists of Competencies, Key 
Skill Areas, Tasks, and Related Job Titles. And links to education resources.

There’s a lot there: hundreds of descriptors. It occurred to me that it might 
be useful to have this in a machine-readable format, but that isn’t available. 
Until now, anyway. The vast majority of it, save the education resources, 
which seem likely to change frequently, has been (tediously) added to a couple 
of JSON files. Warts and all – there are a few instances where they have 
truncated lines and whatnot.

The files will change, in that some fields may be added. Under specialty, if 
you open, say, Tasks, you find a top line that says something like 
"Professionals involved in this Specialty Area perform the following tasks:" 
Other times it says "experts".  I’m not sure it would be a useful thing to 
include.

The structure will likely change at some point: I’ve committed the folly of 
creating a data structure with no idea how it will be used. My hope is that an 
HR professional will see it, it will spark an idea, and then an application 
will drive the data structure, as it should. Which would make the tedium 
worthwhile.

It’s also a serious eye-opener to security generalists such as myself, which 
is another reason I did. JSON does make it possible to see it as a whole, 
which you can’t do by clicking around in the NICCS  Web site. We are well past 
any hope of a single person, no matter how dedicated, being able to  encompass 
more than a small fraction of the skills that the field needs.

Hopefully, now that this is out there, I can use some of my scant spare time 
to get some work done that should live in various private projects under 
gitlab.com/secinfo/hr. 
